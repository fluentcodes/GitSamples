# Topic Branch: Master

The **sandbox** repository should show how you can use git to
* gather codes from experiment in working tests
* divide it in **topic branches**
* share it with others

If you like to extend this repository you're welcome. Actually it's focus are my topics. 

You find only this README on the master **topic branch**. 

## Recent
March 2022
* [java-springboot-objects](../../tree/java-springboot-objects): Added different component examples.
* [java-libs-json-jackson](../../tree/java-libs-json-jackson): Updated it with typed maps and lists.
  * Added typed [map](../../blob/java-libs-json-jackson/test/sources/AddressListTest.java) 
  * and [list](../../blob/java-libs-json-jackson/test/sources/AddressMapTest.java) tests.
* [java-springboot-properties](../../tree/java-springboot-properties): Properties and @Values
  * Added [SpringConfigurableEnvironment](../../blob/java-springboot-properties/test/properties/helper/SpringConfigurableEnvironment.java) and 
  * [SpringConfigurableEnvironmentTest](../../blob/java-springboot-properties/test/properties/helper/SpringConfigurableEnvironmentTest.java) for showing all properties with its sources as a map.
* [java-libs-xml-jackson](../../tree/java-libs-xml-jackson): Initial with check of untyped serialization
* [java-libs-xml-jaxb](../../sandbox/tree/java-libs-xml-jaxb): Check of untyped serialization

February 2022
* [java-json-jackson](../../tree/java-libs-json-jackson): Date conversion examples


## Intention
It's intention is to be a kind of memory for experiments, when one need to get into a special topic. Sometimes
frameworks or languages don't work as one expect or a non working solution is published.

Hopefully one find a solution at the end. But the pitfalls, others can fall into too, are lost. One can see this with a lot of
answers on [stackoverflow](https://stackoverflow.com).

* learning from solutions that doesn't work
* add code in a structured manner
* solutions that hopefully immediately work
* free (wiki) documentation from code snippets

With this sandbox repository I just cleaned up some other sandboxes I had before with the concept of the topic map.
The external wiki is the wiki of github.

I filled it initially with some content, but not all.


## Analysis on [GitSamples-GIT](https://github.com/fluentcodes/GitSamples-GIT)
* [Create new Topic Branches with a Clean History](https://github.com/fluentcodes/GitSamples-GIT/tree/switch-orphan-readme)
* [Behaviour of Markdown Link in Github](https://github.com/fluentcodes/GitSamples-GIT/tree/github-readme-links)
* [Automate Git with Aliases](https://github.com/fluentcodes/GitSamples-GIT/tree/automate-git)

### Links
* https://codingsight.com/git-branching-naming-convention-best-practices/
* https://stackoverflow.com/questions/7653483/github-relative-link-in-markdown-file
* https://levelup.gitconnected.com/what-every-good-readme-should-contain-d6a07d1b39f